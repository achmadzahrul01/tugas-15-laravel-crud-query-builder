<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);

Route::get('/register', [AuthController::class, 'daftar']);

Route::post('/welcome', [AuthController::class, 'welcome']);

Route::get('/table', function(){
    return view('page.table');
});

Route::get('/data-table', function(){
    return view('page.dataTable');
});

//CRUD

// C => Create
// menampilkan form untuk membuat data pemain film baru
Route::get('/cast/create', [CastController::class, 'create']);
// menyimpan data baru ke tabel Cast
Route::post('/cast', [CastController::class, 'store']);

// R => Read
// menampilkan list data para pemain film (boleh menggunakan table html atau bootstrap card)
Route::get('/cast', [CastController::class, 'index']);
// menampilkan detail data pemain film dengan id tertentu
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

// U => Update
// menampilkan form untuk edit pemain film dengan id tertentu
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
// menyimpan perubahan data pemain film (update) untuk id tertentu
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

// D => Delete
// menghapus data pemain film dengan id tertentu
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);

