@extends('layouts.master')

@section('title')
Halaman Edit Cast
@endsection

@section('content')
<form method="post" action="/cast/{{$cast->id}}">
    {{-- validation --}}
    @method('put')
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
    @csrf
    {{-- form input --}}
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="text" value="{{$cast->nama}}" name="nama" class="form-control">
    </div>
    <div class="form-group">
        <label>Umur Cast</label>
        <input type="integer" value="{{$cast->umur}}" name="umur" class="form-control">
    </div>
    <div class="form-group">
        <label>Bio Cast</label>
        <textarea name="bio" class="form-control" cols="30" rows="10">{{$cast->bio}}</textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection