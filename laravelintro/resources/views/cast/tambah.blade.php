@extends('layouts.master')

@section('title')
Halaman Tambah Cast
@endsection

@section('content')
<form method="post" action="/cast">
    {{-- validation --}}
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
    @csrf
    {{-- form input --}}
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="text" name="nama" class="form-control">
    </div>
    <div class="form-group">
        <label>Umur Cast</label>
        <input type="integer" name="umur" class="form-control">
    </div>
    <div class="form-group">
        <label>Bio Cast</label>
        <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection