@extends('layouts.master')

@section('title')
Halaman Tampil Cast
@endsection

@section('content')
<a href='/cast/create' class="btn btn-sm btn-primary">Tambah</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <th scope="row">{{$key + 1}}</th>
            <td>{{$item->nama}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="post">
                    @csrf
                    @method('delete')
                    <a href="cast/{{$item->id}}" class="btn btn-sm btn-info">Detail</a>
                    <a href="cast/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
                    <input type="submit" class="btn btn-sm btn-danger" value="Delete">
                </form>
            </td>
        </tr>
        @empty
            <p>no users</p>
        @endforelse

    </tbody>
  </table>
@endsection