@extends('layouts.master')

@section('title')
Halaman Register
@endsection

@section('content')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
      @csrf

      <label>First Name:</label><br>
      <input type="text" name="fname"><br><br>

      <label>Last Name:</label><br>
      <input type="text" name="lname"><br><br>

      <label>Gender:</label><br><br>

      <input type="radio" value="1" name="status" />Male<br><br>
      <input type="radio" value="2" name="status" />Female<br><br>
      <input type="radio" value="3" name="status" />Other<br><br>

      <label>Nationality:</label><br><br>

      <select name="nationality">
        <option value="1">Indonesian</option>
        <option value="2">Singaporean</option>
        <option value="3">Malaysian</option>
        <option value="4">Australian</option></select
      ><br><br>

      <label>Languange Spoken:</label><br><br>

      <input type="checkbox" value="1" name="languangespoken" />Bahasa Indonesia<br>
      <input type="checkbox" value="2" name="languangespoken" />English<br>
      <input type="checkbox" value="3" name="languangespoken" />Arabic<br>
      <input type="checkbox" value="4" name="languangespoken" />Japanese<br><br>

      <label>Bio:</label><br><br>

      <textarea name="address" id="" cols="30" rows="10"></textarea><br>

      <input type="submit" value="Kirim">

    </form>
@endsection