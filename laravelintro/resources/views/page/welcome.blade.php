@extends('layouts.master')

@section('title')
Halaman Welcome
@endsection

@section('content')
    <h1>Selamat Datang {{$fname}} {{$lname}}</h1>
    <h2>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</h2>
@endsection